# Yellow React Example #

Example React/Redux application. It's grossly over-engineered for what it actually does, but it's supposed to serve as an example for larger projects.

### How do I get set up? ###

Clone the repo and then 

`npm install`

To set get the necessary packages.

`npm run build`

builds the front end.

`npm test`

will run the unit tests.

`npm start`

will start up the server. See the result at http://localhost:3000