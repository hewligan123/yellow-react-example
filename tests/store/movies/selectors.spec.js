/* globals describe, it, beforeEach */

import 'babel-polyfill';

import { expect } from 'chai';
import faker from 'faker';
import { fromJS } from 'immutable';

import { getSortedMovies } from '../../../client/src/store/movies/selectors';

describe('Movie Selector', () => {
    let state;

    beforeEach(() => {
        const testMovies = [];

        for (let i = 0; i < 10; i += 1) {
            testMovies.push({
                rank: i + 1,
                title: faker.lorem.sentence(),
                year: faker.random.number(),
                director: `${faker.name.firstName()} ${faker.name.lastName()}`,
            });
        }

        state = fromJS({
            movies: {
                sortField: 'rank',
                sortDirection: 'forwards',
                movies: testMovies,
            },
        });
    });

    it('Should sort number fields correctly', () => {
        const result = getSortedMovies(state);

        result.forEach((movie, index) => {
            const nextMovie = result[index + 1];

            if (nextMovie) {
                expect(movie.rank < nextMovie.rank).to.be.ok;
            }
        });
    });

    it('Should sort string fields correctly', () => {
        const newState = state.setIn(['movies', 'sortField'], 'title');

        const result = getSortedMovies(newState);

        result.forEach((movie, index) => {
            const nextMovie = result[index + 1];

            if (nextMovie) {
                expect(nextMovie.title.localeCompare(movie.title)).to.equal(1);
            }
        });
    });

    it('Should reverse search when direction is reversed', () => {
        const newState = state.setIn(['movies', 'sortDirection'], 'reverse');

        const result = getSortedMovies(newState);

        result.forEach((movie, index) => {
            const nextMovie = result[index + 1];

            if (nextMovie) {
                expect(movie.rank > nextMovie.rank).to.be.ok;
            }
        });
    });
});
