/* globals describe, it */

import 'babel-polyfill';

import { expect } from 'chai';
import { call, put } from 'redux-saga/effects';

import sagaMaker from '../../../client/src/store/movies/sagas';
import * as actionTypes from '../../../client/src/store/movies/actionTypes';
import * as actionCreators from '../../../client/src/store/movies/actionCreators';
import { getMovieData } from '../../../client/src/api/data-api';

describe('Movies Saga', () => {
    const saga = sagaMaker();
    let loadMoviesSaga;

    it('Should call the load movies saga when it recieves a load movies action', () => {
        const result = saga.next(actionCreators.loadMovies());

        loadMoviesSaga = result.value.FORK.args[1]();

        expect(result.value.FORK.args[0]).to.equal(actionTypes.LOAD_MOVIES);
    });

    it('Should call the data api to load the data', () => {
        const expecteResult = call(getMovieData);

        expect(loadMoviesSaga.next().value).to.deep.equal(expecteResult);
    });

    it('Should yield an action with the movie data', () => {
        const movies = [1, 2, 3, 4];
        const expecteResult = put(actionCreators.moviesLoaded(movies));

        expect(loadMoviesSaga.next(movies).value).to.deep.equal(expecteResult);
    });
});
