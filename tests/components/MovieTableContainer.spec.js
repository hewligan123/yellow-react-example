/* globals describe, it, beforeEach */

import 'babel-polyfill';

import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { fromJS } from 'immutable';

import { expect } from 'chai';
import { spy } from 'sinon';
import { mount } from 'enzyme';

import MovieTableContainer from '../../client/src/components/MovieTableContainer/MovieTableContainer';
import MovieTable from '../../client/src/components/MovieTable/MovieTable';
import Loading from '../../client/src/components/Loading/Loading';
import LoadingError from '../../client/src/components/LoadingError/LoadingError';

describe('MovieTableContainer', () => {
    let state;

    beforeEach(() => {
        state = {
            movies: {
                loading: false,
                sortField: 'rank',
                sortDirection: 'forwards',
                movies: [],
                error: false,
            },
        };
    });

    it('Should create a MovieTable', () => {
        const store = createStore(() => fromJS(state));

        const movieTableContainer = mount(
            <Provider store={store}>
                <MovieTableContainer />
            </Provider>,
        );

        expect(movieTableContainer.find(MovieTable)).to.have.length(1);
    });

    it('Should show a loader if data is loading', () => {
        state.movies.loading = true;

        const store = createStore(() => fromJS(state));

        const movieTableContainer = mount(
            <Provider store={store}>
                <MovieTableContainer />
            </Provider>,
        );

        expect(movieTableContainer.find(Loading)).to.have.length(1);
    });

    it('Should show an error if an error is set', () => {
        state.movies.error = new Error('test error');

        const store = createStore(() => fromJS(state));

        const movieTableContainer = mount(
            <Provider store={store}>
                <MovieTableContainer />
            </Provider>,
        );

        expect(movieTableContainer.find(LoadingError)).to.have.length(1);
    });

    it('Should dispatch a message to the store when the sort buttons are clicked', () => {
        const store = createStore(() => fromJS(state));

        const dispatchSpy = spy(store, 'dispatch');

        const movieTableContainer = mount(
            <Provider store={store}>
                <MovieTableContainer />
            </Provider>,
        );

        movieTableContainer.find('button')
            .first()
            .simulate('click');

        expect(dispatchSpy.called).to.be.ok;
    });
});
