import React from 'react';

class PageHeader extends React.Component {
    render() {
        return (
            <div className="jumbotron">
                <div className="container">
                    <h1>Top Movies</h1>
                </div>
            </div>
        );
    }
}

export default PageHeader;
