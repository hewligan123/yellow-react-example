import React from 'react';
import PropTypes from 'prop-types';

import columnTitles from '../../util/columnTitles';

const reverseSort = currentSort => (currentSort === 'forwards' ? 'reverse' : 'forwards');

class MovieTable extends React.Component {
    getSortIcon(key) {
        if (key === this.props.sortField) {
            if (this.props.sortDirection === 'forwards') {
                return (<i className="glyphicon glyphicon-chevron-up" />);
            }

            return (<i className="glyphicon glyphicon-chevron-down" />);
        }

        return (<i className="glyphicon glyphicon-sort" />);
    }

    sort(key) {
        let sortDirection = 'forwards';

        if (key === this.props.sortField) {
            sortDirection = reverseSort(this.props.sortDirection);
        }

        this.props.setSort(key, sortDirection);
    }

    render() {
        return (
            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            {
                                Object.entries(columnTitles).map(title => (
                                    <th key={title[0]}>
                                        <button onClick={this.sort.bind(this, title[0])} className="btn btn-xs btn-primary">{this.getSortIcon(title[0])}</button>
                                        &nbsp;{title[1]}
                                    </th>
                                ))
                            }
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.movies.map(movie => (<tr key={movie.title}>
                                {
                                    Object.keys(columnTitles).map(key => <td key={key}>{movie[key]}</td>)
                                }
                            </tr>))
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

MovieTable.propTypes = {
    movies: PropTypes.array.isRequired,
    sortDirection: PropTypes.string.isRequired,
    sortField: PropTypes.string.isRequired,
    setSort: PropTypes.func.isRequired,
};

export default MovieTable;
