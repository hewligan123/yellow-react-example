import React from 'react';

class LoadError extends React.Component {
    render() {
        return (
            <div className="error-container alert alert-danger">
                <h2>A loading error occurred.</h2>
            </div>
        );
    }
}

export default LoadError;
