import React from 'react';

import PageHeader from '../PageHeader/PageHeader';
import MovieTableContainer from '../MovieTableContainer/MovieTableContainer';

class Page extends React.Component {
    render() {
        return (
            <div>
                <PageHeader />
                <MovieTableContainer />
            </div>
        );
    }
}

export default Page;
