import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getSortedMovies } from '../../store/selectors';
import { loadMovies, setSortOrder } from '../../store/actions';

import MovieTable from '../MovieTable/MovieTable';
import Loading from '../Loading/Loading';
import LoadingError from '../LoadingError/LoadingError';

class MovieTableContainer extends React.Component {
    componentDidMount() {
        this.props.loadMovies();
    }

    render() {
        return (
            <div>
                {this.props.loading ? <Loading /> : ''}

                {this.props.error ? <LoadingError /> : ''}

                {!this.props.loading && !this.props.error ? (
                    <MovieTable
                        movies={this.props.movies}
                        setSort={this.props.setSort}
                        sortField={this.props.currentSortField}
                        sortDirection={this.props.currentSortDirection}
                    />
                ) : ''}
            
            </div>
        );
    }
}

MovieTableContainer.propTypes = {
    movies: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.instanceOf(Error)]).isRequired,
    currentSortDirection: PropTypes.string.isRequired,
    currentSortField: PropTypes.string.isRequired,
    loadMovies: PropTypes.func.isRequired,
    setSort: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => {
    return {
        currentSortField: state.getIn(['movies', 'sortField']),
        currentSortDirection: state.getIn(['movies', 'sortDirection']),
        movies: getSortedMovies(state),
        loading: state.getIn(['movies', 'loading']),
        error: state.getIn(['movies', 'error']),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadMovies() {
            dispatch(loadMovies());
        },

        setSort(sortField, sortDirection) {
            dispatch(setSortOrder({ sortField, sortDirection }));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieTableContainer);
