// This would normally be a place to export all action creators for different
// state slices. But we only have 1 in this case.

export * from './movies/selectors';
