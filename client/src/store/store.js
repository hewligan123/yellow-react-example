import { createStore, applyMiddleware } from 'redux';
import { combineReducers } from 'redux-immutable';

import createSagaMiddleware from 'redux-saga';

import movieReducer from './movies/reducer';
import movieSaga from './movies/sagas';

const sagaMiddleware = createSagaMiddleware();

// combineReducers is normally needed when we wish to use multiple reducers to
// handle different slices of state. In this case, there is actually only 1 slice,
// but combineReducers is left as an example.
const store = createStore(
    combineReducers({
        movies: movieReducer, 
    }),
    applyMiddleware(
        sagaMiddleware,
    ),
);

sagaMiddleware.run(movieSaga);

export default store;
