import * as actionTypes from './actionTypes';

export const loadMovies = () => {
    return {
        type: actionTypes.LOAD_MOVIES,
    };
};

export const moviesLoaded = (movieList) => {
    return {
        type: actionTypes.MOVIES_LOADED,
        payload: movieList,
    };
};

export const movieLoadError = (error) => {
    return {
        type: actionTypes.MOVIE_LOAD_ERROR,
        payload: error,
        error: true,
    };
};

export const setSortOrder = ({ sortField, sortDirection } = { sortfield: 'rank', sortDirection: 'forwards' }) => {
    return {
        type: actionTypes.SET_SORT_ORDER,
        payload: {
            sortField,
            sortDirection,
        },
    };
};
