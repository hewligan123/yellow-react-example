import { createSelector } from 'reselect';

const getSortField = state => state.getIn(['movies', 'sortField']);
const getSortDirection = state => state.getIn(['movies', 'sortDirection']);
const getMovies = state => state.getIn(['movies', 'movies']).toJS();

// Would normally have multiple exports here, but because this example only have 1, so...
// eslint-disable-next-line import/prefer-default-export
export const getSortedMovies = createSelector(
    [
        getSortField,
        getSortDirection,
        getMovies,
    ],
    (sortField, sortDirection, movies) => {
        const sortedMovies = movies.sort((movieA, movieB) => {
            if (typeof movieA[sortField] === 'string') {
                return movieA[sortField].localeCompare(movieB[sortField]);
            }

            return movieA[sortField] - movieB[sortField];
        });

        if (sortDirection === 'reverse') {
            sortedMovies.reverse();
        }

        return sortedMovies;
    },
);

