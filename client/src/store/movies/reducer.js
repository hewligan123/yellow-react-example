import { Record, List, fromJS } from 'immutable';

import * as actionTypes from './actionTypes';

const MovieStore = Record({
    loading: false,
    sortField: 'rank',
    sortDirection: 'forwards',
    movies: List(),
    error: false,
});

export default (state = new MovieStore(), action) => {
    switch (action.type) {
        case actionTypes.LOAD_MOVIES:
            return state.set('loading', true)
                .set('error', false);

        case actionTypes.MOVIES_LOADED:
            return state.set('loading', false)
                .set('error', false)
                .set('movies', fromJS(action.payload));

        case actionTypes.MOVIE_LOAD_ERROR:
            return state.set('error', action.payload)
                .set('loading', false);

        case actionTypes.SET_SORT_ORDER:
            return state.set('sortField', action.payload.sortField)
                .set('sortDirection', action.payload.sortDirection);

        default:
            return state;
    }
};
